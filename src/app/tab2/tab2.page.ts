import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ClientModel } from "../models/client-item.model";
import { FitcoService } from "../services/fitco.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"],
})
export class Tab2Page {
  client: ClientModel = new ClientModel();
  constructor(
    private fitco: FitcoService,
    private router: ActivatedRoute,
    private route: Router
  ) {
    const id = router.snapshot.paramMap.get("id");
    if (id !== "new") {
      this.fitco.getClient(id).subscribe((resp: ClientModel) => {
        this.client = resp;
        this.client.id = id;
      });
    }
  }

  saveInfo(form: NgForm) {
    if (form.invalid) {
      return;
    }

    if (this.client.id) {
      this.fitco.updateClient(this.client).subscribe((resp) => {
        this.route.navigate(["/tabs/tab1/client/", this.client.id]);
      });
    } else {
      this.fitco.createClient(this.client).subscribe((resp) => {
        form.reset();
        this.route.navigate(["/tabs/tab1"]);
      });
    }
  }
}
