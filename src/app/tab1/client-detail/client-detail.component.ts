import { Component, OnInit } from "@angular/core";
import { ClientModel } from "src/app/models/client-item.model";
import { FitcoService } from "src/app/services/fitco.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { AngularFireDatabase } from "@angular/fire/database";
import { map } from "rxjs/operators";

@Component({
  selector: "app-client-detail",
  templateUrl: "./client-detail.component.html",
  styleUrls: ["./client-detail.component.scss"],
})
export class ClientDetailComponent implements OnInit {
  client: ClientModel = new ClientModel();
  loading = true;
  constructor(
    private fitco: FitcoService,
    private routerLink: ActivatedRoute,
    public alertController: AlertController,
    private route: Router,
    private db: AngularFireDatabase
  ) {}

  ngOnInit() {
    const id = this.routerLink.snapshot.paramMap.get("id");
    this.db
      .object("clients")
      .valueChanges()
      .subscribe((resp) => {
        this.replaceContentToClient(resp, id);
      });

    if (id) {
      this.fitco.getClient(id).subscribe((resp: ClientModel) => {
        this.client = resp;
        this.client.id = id;
        this.loading = false;
      });
    }
  }

  replaceContentToClient(clients: Object, id: string) {
    Object.keys(clients).forEach((key) => {
      if (key === id) {
        this.client = clients[key];
        this.client.id = key;
      }
    });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      header: "Confirm!",
      message: `Are you sure to delete <strong>${this.client.name}</strong>!!!`,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "secondary",
          handler: (blah) => {
            console.log("Confirm Cancel: blah");
          },
        },
        {
          text: "Okay",
          handler: () => {
            console.log("Confirm Okay");
            this.fitco.deleteClient(this.client.id).subscribe();
            this.route.navigate(["/tabs/tab1"]);
          },
        },
      ],
    });

    await alert.present();
  }

  goToEditPage() {
    this.route.navigate(["/tabs/tab2", this.client.id]);
  }

  backListClients() {
    this.route.navigate(["/tabs/tab1"]);
  }
}
