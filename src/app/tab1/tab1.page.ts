import { Component } from "@angular/core";
import { FitcoService } from "../services/fitco.service";
import { ClientModel } from "../models/client-item.model";
import { AngularFireDatabase } from "@angular/fire/database";

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"],
})
export class Tab1Page {
  clients: ClientModel[] = [];
  loading = true;

  constructor(private fitco: FitcoService, private db: AngularFireDatabase) {
    db.list("clients")
      .valueChanges()
      .subscribe((resp: ClientModel[]) => {
        this.clients = resp;
        this.insertIdToClients(this.clients);
        this.loading = false;
      });
  }

  insertIdToClients(clients: ClientModel[]) {
    this.db
      .list("clients")
      .snapshotChanges()
      .subscribe((resp: any) =>
        resp.forEach((value: any, index) => (clients[index].id = value.key))
      );
  }
}
