import { TestBed } from '@angular/core/testing';

import { FitcoService } from './fitco.service';

describe('FitcoService', () => {
  let service: FitcoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FitcoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
