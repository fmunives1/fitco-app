import { Injectable } from "@angular/core";
import { ClientModel } from "../models/client-item.model";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class FitcoService {
  API = "https://fitco-5eba0.firebaseio.com";

  constructor(private http: HttpClient) {}

  getAllClients() {
    return this.http
      .get(`${this.API}/clients.json`)
      .pipe(map(this.convertToArray));
  }

  getClient(id: string) {
    return this.http.get(`${this.API}/clients/${id}.json`);
  }

  createClient(client: ClientModel) {
    return this.http.post(`${this.API}/clients.json`, client);
  }

  updateClient(client) {
    const copyClient = { ...client };
    delete copyClient.id;
    return this.http.patch(`${this.API}/clients/${client.id}.json`, copyClient);
  }

  deleteClient(id: string) {
    return this.http.delete(`${this.API}/clients/${id}.json`);
  }

  private convertToArray(clientObject: Object) {
    const arrClients = [];
    Object.keys(clientObject).forEach((key) => {
      let client = clientObject[key];
      client.id = key;
      arrClients.push(client);
    });
    return arrClients;
  }
}
