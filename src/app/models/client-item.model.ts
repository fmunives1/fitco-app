export class ClientModel {
  id: string;
  name: string;
  address: string;
  birthday: string;
  isActive: boolean;

  constructor() {
    this.isActive = true;
  }
}
