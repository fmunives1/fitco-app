// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDrd1YeK1prTZfqvfBgrQoRziuuHEunqVE",
    authDomain: "fitco-5eba0.firebaseapp.com",
    databaseURL: "https://fitco-5eba0.firebaseio.com",
    projectId: "fitco-5eba0",
    storageBucket: "fitco-5eba0.appspot.com",
    messagingSenderId: "318787393788",
    appId: "1:318787393788:web:915b1c7c5d96b68ca6df79",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
